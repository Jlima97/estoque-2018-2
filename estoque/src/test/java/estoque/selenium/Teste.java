package estoque.selenium;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
public class Teste {


	@Test
	public void testarChrome() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:/Users/100933024/Desktop/chromedriver.exe");
		System.setProperty("webdriver.firefox.driver", "C:/Users/100933024/Downloads/geckodriver.exe");

		

		
		WebDriver driver = new ChromeDriver();
		
		driver.get("C:/Users/100933024/git/estoque-2018-2/estoque/src/main/webapp/lista-compras.html");
						
		Select select = new Select(driver.findElement(By.id("produto")));
		select.selectByVisibleText("Manga");		
		
		WebElement quantidade = driver.findElement(By.id("quantidade"));
		quantidade.sendKeys("10");
		
		WebElement valor = driver.findElement(By.id("valorUnitario"));
		valor.sendKeys("5");
		
		WebElement calcular = driver.findElement(By.id("calcularBtn"));
		calcular.click();
		
		WebElement total = driver.findElement(By.id("valorTotal"));
		
	    Assert.assertNotEquals(total, 50);
		
		
	}
	

}

